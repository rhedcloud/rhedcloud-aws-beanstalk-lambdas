import os
import sys
from time import sleep
import time
import boto3
from botocore.exceptions import ClientError

#This function will terminate instances and set rolling updates to disabled
def terminate_instances(ApplicationName, EnvironmentName):

    try:
        client = boto3.client('elasticbeanstalk')
    except ClientError as err:
        print("Failed to create boto3 client \n" + str(err))

    try:
        response = client.update_environment(
            ApplicationName= ApplicationName,
            EnvironmentName= EnvironmentName,
            OptionSettings = [
                {
                     'Namespace': 'aws:autoscaling:asg',
                     'OptionName': 'MinSize',
                     'Value': '1'
                },
                {
                     'Namespace': 'aws:autoscaling:asg',
                     'OptionName': 'MaxSize',
                     'Value': '1'
                },
                {
                     'Namespace': 'aws:autoscaling:updatepolicy:rollingupdate',
                     'OptionName': 'RollingUpdateEnabled',
                     'Value': 'false'
                },
            ])

    except ClientError as err:
        print("Failed to update environment.\n" + str(err))


#This function will recreate instances and set rolling updates to enabled
def recreate_instances(ApplicationName, EnvironmentName):

        try:
            client = boto3.client('elasticbeanstalk')
        except ClientError as err:
            print("Failed to create boto3 client \n" + str(err))

        try:
            response = client.update_environment(
                ApplicationName = ApplicationName,
                EnvironmentName = EnvironmentName,
                OptionSettings = [
                    {
                         'Namespace': 'aws:autoscaling:asg',
                         'OptionName': 'MinSize',
                         'Value': '2'
                    },
                    {
                         'Namespace': 'aws:autoscaling:asg',
                         'OptionName': 'MaxSize',
                         'Value': '2'
                    },
                    {
                         'Namespace': 'aws:autoscaling:updatepolicy:rollingupdate',
                         'OptionName': 'RollingUpdateEnabled',
                         'Value': 'true'
                    },
                ])

        except ClientError as err:
            print("Failed to update environment.\n" + str(err))


#This function will act as the main function during the execution
def lambda_handler(event, context):

    ApplicationName= event['ApplicationName']
    EnvironmentName= event['EnvironmentName']

    print ("Updating the beanstalk configuration")
    print ("...")
    print ("Disabling rolling updates and setting number of instances to 0")
    terminate_instances(ApplicationName, EnvironmentName)
    time.sleep(180)
    print ("Enabling rolling updates and setting number of instances to 2. Both minimum and maximum")
    recreate_instances(ApplicationName, EnvironmentName)
    time.sleep(240)
    print ("Disabling rolling updates and setting number of instances to 0 again to terminate the old instance")
    terminate_instances(ApplicationName, EnvironmentName)
    time.sleep(180)
    print ("Enabling rolling updates and setting number of instances to 2. Both minimum and maximum")
    recreate_instances(ApplicationName, EnvironmentName)
    time.sleep(180)
    print ("Beanstalk environment update complete")
